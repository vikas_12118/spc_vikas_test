var baseUrl = "http://localhost:8080";
var _addSalesDetailsUrl = baseUrl + "/sales";

var addSalesDetails = function () {
    //console.log(_addSalesDetailsUrl);
    if (!$('#name').val().trim()) {
        alert("Enter the name");
        return;
    }
    if (!$('#date').val().trim()) {
        alert("Enter the date");
        return;
    }
    if (!$('#value').val().trim()) {
        alert("Enter the value");
        return;
    }
    if (!$('#location').val().trim()) {
        alert("Enter the location");
        return;
    }
    let salesDetails = {
        salesmanName: $('#name').val(),
        createdDate: $('#date').val(),
        saleValue: $('#value').val(),
        location: $("#location").val()

    };
    console.log("salesDetails:", salesDetails);
    $.ajax({
        type: "POST",
        url: _addSalesDetailsUrl,
        data: JSON.stringify(salesDetails),
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            console.log('sales added successfully ...' + JSON.stringify(data));
            $('#name').val('');
            $('#date').val('');
            $('#value').val('');
            $("#location").val('');
        },
        complete: function () {
            fetchSalesData();
        },
        error: function (httpObj, textStatus) {
            console.log("error" + JSON.stringify(httpObj));
        }

    });
};

var fetchSalesData = () => {
    console.log("fetching sales data.....");

    $.ajax({
        type: "GET",
        url: _addSalesDetailsUrl,
        processData: false,
        contentType: 'application/json',
        dataType: 'json',
        success: function (resData) {
            console.log('fetch data successfully ...' + JSON.stringify(resData));
            let labels = resData.label;
            let data = resData.data;
            if (labels.length === 0 && data.length === 0) {
                $('#datamsg').show();
            }else {
                $('#datamsg').hide();
            }
            document.getElementById('canvas').remove();
            $('#canvascontainerbox').append('<canvas id="canvas" style="height: 300px;width: 95%;cursor: pointer;"></canvas>');
            var ctx = document.getElementById('canvas').getContext('2d');
            Chart.defaults.LineWithLine = Chart.defaults.line;
            Chart.controllers.LineWithLine = Chart.controllers.line.extend({
                draw: function(ease) {
                    Chart.controllers.line.prototype.draw.call(this, ease);
                    if (this.chart.tooltip._active && this.chart.tooltip._active.length) {
                        var activePoint = this.chart.tooltip._active[0],
                            ctx = this.chart.ctx,
                            x = activePoint.tooltipPosition().x,
                            topY = this.chart.scales['y-axis-0'].top,
                            bottomY = this.chart.scales['y-axis-0'].bottom;
                        // draw line
                        ctx.save();
                        ctx.beginPath();
                        ctx.moveTo(x, topY);
                        ctx.lineTo(x, bottomY);
                        ctx.lineWidth = 2;
                        ctx.strokeStyle = '#07C';
                        ctx.stroke();
                        ctx.restore();
                    }
                }
            });
             var chart = new Chart(ctx, {
                 type: 'LineWithLine',
                 data: {
                     labels: labels,
                     datasets: [{
                         lineTension: 0,
                         backgroundColor: "rgb(34,139,34)",
                         borderColor: "rgb(34,139,34)",
                         data: data,
                         fill: false,
                         pointRadius: 1.5,
                         pointHoverRadius: 1,
                         borderWidth :1.5
                     }],
                 },
                 options: {
                     maintainAspectRatio: false,
                     responsive: false,
                     hover: {
                         mode: 'index',
                         intersect: false,
                     },
                     title: {
                         display: true,
                         text: ''
                     },
                     legend: {
                         display: false
                     },
                     tooltips: {
                         mode: 'index',
                         intersect: false,
                     },
                     scales: {
                         yAxes: [{
                             scaleLabel: {
                                 display: true,
                                 labelString: 'Sales Value'
                             }
                         }],
                         xAxes: [{
                             scaleLabel: {
                                 display: true,
                                 labelString: 'SalesMan'
                             }
                         }],
                     }
                 }
             })
        },
        error: function (httpObj, textStatus) {
            console.log("error" + JSON.stringify(httpObj));
        }

    });
}

$(document).ready(function() {
    fetchSalesData();
});

