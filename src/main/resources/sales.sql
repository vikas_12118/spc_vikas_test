DROP TABLE IF EXISTS arithmetic;

CREATE TABLE arithmetic (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  salesman_name VARCHAR(45) NOT NULL,
  created_date DATE NOT NULL,
  sale_value INTEGER NOT NULL ,
  location VARCHAR(225) NOT NULL
);