package com.spc.test.controller;

import com.spc.test.constants.APIEndpoints;
import com.spc.test.dtos.SalesData;
import com.spc.test.model.SalesEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@RestController
public class UserController extends BaseController {
    @PostMapping(APIEndpoints.SALES_DETAILS)
    public ResponseEntity<SalesEntity> saveSalesDetails(@RequestBody SalesEntity salesDetails) {
        log.info("POST method called for "+APIEndpoints.SALES_DETAILS);
        SalesEntity sales =  userService.saveSalesDetails(salesDetails);
        return new ResponseEntity<>(sales, HttpStatus.OK);
    }

    @GetMapping(APIEndpoints.SALES_DETAILS)
    public ResponseEntity<SalesData> getSalesData() {
        log.info("GET method called for "+APIEndpoints.SALES_DETAILS);
        SalesData salesData = userService.getSalesData();
        return new ResponseEntity<>(salesData, HttpStatus.OK);
    }
}
