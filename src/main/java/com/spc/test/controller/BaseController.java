package com.spc.test.controller;

import com.spc.test.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseController {

    @Autowired
    public UserService userService;
}
