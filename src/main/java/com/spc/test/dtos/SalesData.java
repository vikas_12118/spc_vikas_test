package com.spc.test.dtos;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SalesData {
    private List<String> label;
    private List<String> data;
}
