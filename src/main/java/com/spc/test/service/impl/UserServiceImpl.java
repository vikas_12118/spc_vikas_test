package com.spc.test.service.impl;

import com.spc.test.dtos.SalesData;
import com.spc.test.model.SalesEntity;
import com.spc.test.service.BaseService;
import com.spc.test.service.UserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl extends BaseService implements UserService {
    @Override
    public SalesEntity saveSalesDetails(SalesEntity salesDetails) {
        return salesRepository.save(salesDetails);
    }

    @Override
    public SalesData getSalesData() {
        List<SalesEntity> sales = salesRepository.findAll();
        List<String> salesmanName = new ArrayList<>();
        List<String> saleValue = new ArrayList<>();
        SalesData salesData = new SalesData();
        for (SalesEntity sale : sales) {
            salesmanName.add(sale.getSalesmanName());
            saleValue.add(String.valueOf(sale.getSaleValue()));
        }
        salesData.setLabel(salesmanName);
        salesData.setData(saleValue);
        return salesData;
    }
}
