package com.spc.test.service;

import com.spc.test.dtos.SalesData;
import com.spc.test.model.SalesEntity;

public interface UserService {
    public SalesEntity saveSalesDetails(SalesEntity salesEntity);
    public SalesData getSalesData();
}
