package com.spc.test.service;

import com.spc.test.repositories.SalesRepository;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseService {

    @Autowired
    public SalesRepository salesRepository;
}
